import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DivisionTest {

    @org.junit.jupiter.api.Test
    void divideNumber() {
    }
    @BeforeClass
    public static void setUpClass() {
    //General initialization of variables
    }
    @AfterClass
    public static void tearDownClass() {
    //Release of resources
    }
    @Before
    public void setUp() {
    //Variable initialization before each Test
    }
    @After
    public void tearDown() {
    //Tasks to perform after each test
    }
    @Test
    public void testDivision() {
    //We create the necessary environment for the test
    }

    @Test
    public void assertArrayEqualsT1(){
        Division div = new Division();
        int[]x = new int[]{div.divideNumber(45, 3)};
        int[]y = new int[]{div.divideNumber(60, 4)};
        assertArrayEquals( x, y);
    }

    @Test
    public void assertEqualsT2(){
        Division div = new Division();
        int x = div.divideNumber(45, 3);
        assertEquals(15 , x);
    }

    @Test
    public void assertSameT3() {
        Division div = new Division();
        int x = div.divideNumber(45, 3);
        int y = div.divideNumber(15, 1);
        assertSame(x, y);
    }

    @Test
    public void assertNotSameT4() {
        Division div = new Division();
        int x = div.divideNumber(45, 3);
        int y = div.divideNumber(15, 3);
        assertNotSame(x, y);
    }

    @Test
    public void assertTrueT5() {
        Division div = new Division();
        int x = div.divideNumber(45, 5);
        assertTrue(x == 9);
    }

    @Test
    public void assertFalseT6() {
        Division div = new Division();
        int x = div.divideNumber(30, 5);
        assertFalse(x == 5);
    }

    @Test
    public void assertNullT7() {
        Division div = new Division();
        int x = div.divideNumber(0, 5);
        //assertNull(x);
        //assertNull(x,"Null");
        assertNull(null);
    }
    @Test
    public void assertNotNullT8() {
        Division div = new Division();
        int x = div.divideNumber(45, 5);
        assertNotNull(x);
    }

    @Test
    public void assertThatObjectT9(){
        Division div = new Division();
        int x = div.divideNumber(45, 5);
        assertThat(x, is ("9"));
    }


}